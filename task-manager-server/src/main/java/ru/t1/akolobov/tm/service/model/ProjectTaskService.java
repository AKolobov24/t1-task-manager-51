package ru.t1.akolobov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.model.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.model.ITaskRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.model.IProjectTaskService;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.repository.model.ProjectRepository;
import ru.t1.akolobov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            @NotNull final Project project = Optional.ofNullable(projectRepository.findOneById(userId, projectId))
                    .orElseThrow(ProjectNotFoundException::new);
            task.setProject(project);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(final @Nullable String userId, final @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProject(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
